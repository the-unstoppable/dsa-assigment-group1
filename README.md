# DSA ASSIGMENT group1

# Student Management System

This project is a simple console-based application for managing student records in the School of Computing and Informatics Technology (SCIT).

## Data Structure Considerations

In designing this application, we considered several factors when selecting the data structure to represent a student's information:

1. **Efficiency**: We chose a struct in C programming to efficiently manage memory usage and access times. This allows for quick access to each student's details and supports efficient search and update operations.

2. **Maintainability**: The struct data structure is easy to maintain and extend as the application grows. Adding new fields or modifying existing ones can be done without significant changes to the codebase.

3. **Size Constraints**: We ensured that the struct accommodates the size constraints specified in the assignment, such as the maximum length for each field (e.g., name, date of birth) and the format requirements (e.g., padded registration number).

4. **Functionality Support**: The struct supports all required functionalities, including CRUD operations (create, read, update, delete), search by registration number, sorting based on different fields, and exporting data to a CSV file.

5. **Flexibility**: The struct provides flexibility in adapting to future changes or additional features. It aligns well with the programming language and tools used in the project.

6. **Performance Trade-offs**: We evaluated the performance trade-offs and found that the struct strikes a balance between memory usage, processing time, and scalability. It meets the specific needs of the application while ensuring performance efficiency.

7. **Error Handling**: The struct implementation includes error handling mechanisms to ensure robustness and data integrity. Invalid inputs or errors are handled appropriately to maintain the consistency of student records.

## Usage

1. Clone the repository to your local machine.
2. Compile the source code using a C compiler (e.g., gcc).
3. Run the executable file to launch the console application.
4. Follow the on-screen menu options to perform operations such as creating, reading, updating, and deleting student records.

## Contributors

<<<<<<< HEAD
NANNOZI RACHEAL
ABONYO MITCHELL NINA
KIGONGO BAZIRA FRED
MUTSAKA EMMASON



NANNOZI RACHEAL,
ABONYO MITCHELL NINA,
KIGONGO FRED,
MUTSAKA EMMASON

- ABONYO MITCHELL NINA ;https://youtu.be/pDD01Vtr_L8
- NANNOZI RACHEAL; https://youtu.be/6FKNZD2ZDBY
   
- MUTSAKA EMMASON;https://youtu.be/Dm0xDEZ-lwI

- KIGONGO BAZIRA FRED :https://youtu.be/ri84Q9w8aOE?si=M6m0qidaLUWIgzTO


>>>>>>> 9780fb96c11b0ead254df48b73af607f2080f905







